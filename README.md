# VBAMathFormatter

A simple VBA macro that can format the mathematical expression in Word similar to LaTeX. It can be useful if the mathematical expressions are simple and only default font styles provided in MS word is allowed when writing manuscript.

Currently implemented:

1.  $ ... $ : Mathematical expression (letters are italic)
2.  ^ | ^{...} : Superscript
3.  _ | _{...} : Subscript

Future implementation:
1.  Mathematical functions: sqrt, exp, log, ln, sin, cos, ...
2.  Some LaTeX-like command: \mathbf{...}, \noformat{}, ...