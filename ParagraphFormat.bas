Attribute VB_Name = "ParagraphFormat"
Option Explicit

Private Enum ChType
    chAlphabet = 1
    chNumber = 2
    chDot = 3
    chOperator = 4
    chSlash = 5
    chParenthesis = 6
    chSpace = 7
    chSpaceCh = 8
    chUnderScore = 9
    chOthers = 100
    
End Enum

Private curCh As Long
Private chCount As Long

Private chDelete() As Long
Private chDeleteCount As Long


Private Function GetCharType(ByVal ch As String) As ChType
    If (ch >= "A" And ch <= "Z") Or (ch >= "a" And ch <= "z") Then
        GetCharType = chAlphabet
    ElseIf (ch >= "0" And ch <= "9") Then
        GetCharType = chNumber
    ElseIf (ch = ".") Then
        GetCharType = chDot
    ElseIf InStr(1, "+-*/^&|!~<>=", ch) Then
        GetCharType = chOperator
    ElseIf InStr(1, "(){}[]", ch) Then
        GetCharType = chParenthesis
    ElseIf ch = "\" Then
        GetCharType = chSlash
    ElseIf ch = " " Then
        GetCharType = chSpace
    ElseIf ch = vbTab Or ch = vbCr Or ch = vbLf Or ch = vbCrLf Then
        GetCharType = chSpaceCh
    ElseIf ch = "_" Then
        GetCharType = chUnderScore
    Else
        GetCharType = chOthers
    End If
    
End Function

Private Function isMathFunction(ByVal s As String) As Boolean
    Dim result As Boolean
    Dim funcListStr As String
    Dim funcList() As String
    Dim i As Integer
    
    result = False
    
    funcListStr = "sqrt|exp|log|ln|sin|cos|tan|cot|sec|csc|sinh|cosh|tanh|coth|sech|csch|asin|acos|atan|acot|asec|acsc|Var"
    funcList = Split(funcListStr, "|")
    
    For i = LBound(funcList) To UBound(funcList)
        If s = funcList(i) Then
            result = True
            Exit For
        End If
    Next i
    isMathFunction = result

End Function

Private Function hasNextCh() As Boolean
    hasNextCh = IIf(curCh < chCount, True, False)
    
End Function

Private Sub consumeCh(Optional numCh As Integer = 1)
    curCh = curCh + numCh
    
End Sub

Private Function getNextCh() As String
    getNextCh = ActiveWindow.Selection.Characters(curCh + 1).Text
    
End Function

Private Sub AddDeleteCh(startPos As Long, Optional endPos As Long = -1)
    Dim i As Integer
    Dim c As Integer
    If endPos = -1 Then
        c = 1
    Else
        c = endPos - startPos + 1
    End If
    ReDim Preserve chDelete(chDeleteCount + c) As Long
    For i = 0 To c - 1
        chDelete(chDeleteCount + i) = startPos + i
    Next i
    chDeleteCount = chDeleteCount + c
    
End Sub

Private Sub sortDeleteCh(lo As Long, hi As Long)
    Dim p As Long
    If lo < hi Then
        p = partitioning(lo, hi)
        sortDeleteCh lo, p - 1
        sortDeleteCh p + 1, hi
    End If
    
End Sub

Private Function partitioning(lo As Long, hi As Long) As Long
    Dim p As Long
    Dim i As Long, j As Long
    Dim tmp As Long
    p = chDelete(hi)
    i = lo - 1
    For j = lo To hi - 1
        If chDelete(j) > p Then
            i = i + 1
            tmp = chDelete(i)
            chDelete(i) = chDelete(j)
            chDelete(j) = tmp
        End If
    Next j
    tmp = chDelete(i + 1)
    chDelete(i + 1) = chDelete(hi)
    chDelete(hi) = tmp
    partitioning = i + 1
    
End Function

Private Sub DeleteCh()
    Dim i As Integer
    Dim j As Integer
    sortDeleteCh LBound(chDelete), UBound(chDelete)
    'Debug.Print "Control Chars"
    For i = LBound(chDelete) To UBound(chDelete)
        'Debug.Print chDelete(i)
        
        If chDelete(i) > 0 And chDelete(i) < chCount Then
            'Debug.Print chDelete(i); ": |" + ActiveWindow.Selection.Characters(chDelete(i)).Text + "|"
            ActiveWindow.Selection.Characters(chDelete(i)).Text = ""
        End If
    Next i
    
End Sub

Private Sub init()
    curCh = 0
    chCount = ActiveWindow.Selection.Characters.Count
    ReDim chDelete(0) As Long
    chDeleteCount = 0
    
End Sub

Private Sub MathFormat(ByVal startPos As Long, ByVal endPos As Long)
    Dim nextCh As String
    Dim tmpBuffer As String
    Dim sPos As Long, ePos As Long
    Dim i As Integer
    curCh = startPos - 1
    With ActiveWindow.Selection
        If startPos <= endPos Then
            Do While hasNextCh
                nextCh = getNextCh
                If GetCharType(nextCh) = chAlphabet Then
                    .Characters(curCh + 1).Font.Italic = True
                    consumeCh
                    
                ElseIf nextCh = "-" Then
                    .Characters(curCh + 1).InsertSymbol 8211, "Times New Roman", True
                    consumeCh
                    
                ElseIf nextCh = "\" Then
                    consumeCh
                    AddDeleteCh curCh
                    If hasNextCh Then
                        nextCh = getNextCh

                        If nextCh = "$" Then
                            consumeCh
'                        ElseIf GetCharType(nextCh) = chAlphabet Then
'                            consumeCh
'                            tmpBuffer = nextCh
'                            Do While hasNextCh
'
'                                nextCh = getNextCh
'                                If GetCharType(nextCh) = chAlphabet Then
'                                    tmpBuffer = tmpBuffer + nextCh
'                                    consumeCh
'                                Else
'                                    Exit Do
'                                End If
'                            Loop
'                            'Debug.Print "|" + tmpBuffer + "|"
'                            If isMathFunction(tmpBuffer) Then
'                                For i = 1 To Len(tmpBuffer)
'                                    consumeCh
'                                Next i
'
'                            ElseIf tmpBuffer = "mathbf" Then
'                                For i = 1 To Len(tmpBuffer)
'                                    consumeCh
'                                Next i
'
'                            ElseIf tmpBuffer = "text" Then
'                                For i = 1 To Len(tmpBuffer)
'                                    consumeCh
'                                Next i
'
'                            Else
'                                MsgBox "Unexpected command: " + tmpBuffer
'                                End
'                            End If
                        End If
                    End If

                ElseIf nextCh = "^" Then
                    consumeCh
                    AddDeleteCh curCh
                    If hasNextCh Then
                        nextCh = getNextCh
                        If nextCh = "{" Then
                            If hasNextCh Then
                                consumeCh
                                AddDeleteCh curCh
                                sPos = curCh
                                ePos = sPos
                                Do While hasNextCh
                                    .Characters(curCh).Font.Superscript = True
                                    nextCh = getNextCh
                                    If nextCh = "}" Then
                                        consumeCh
                                        AddDeleteCh curCh
                                        ePos = curCh - 1
                                        curCh = sPos
                                        MathFormat sPos, ePos
                                        curCh = ePos + 1
                                        Exit Do
                                    Else
                                        consumeCh
                                    End If

                                Loop
                                
                            Else
                                MsgBox "Missing } for superscript"
                                End
                            End If
                        Else
                            .Characters(curCh + 1).Font.Superscript = True
                            consumeCh
                            MathFormat curCh, curCh
                            
                        End If
                    End If
                ElseIf nextCh = "_" Then
                    consumeCh
                    AddDeleteCh curCh
                    If hasNextCh Then
                        nextCh = getNextCh
                        If nextCh = "{" Then
                            If hasNextCh Then
                                consumeCh
                                AddDeleteCh curCh
                                sPos = curCh
                                ePos = sPos
                                Do While hasNextCh
                                    .Characters(curCh).Font.Subscript = True
                                    nextCh = getNextCh
                                    If nextCh = "}" Then
                                        consumeCh
                                        AddDeleteCh curCh
                                        ePos = curCh - 1
                                        curCh = sPos
                                        MathFormat sPos, ePos
                                        curCh = ePos + 1
                                        Exit Do
                                    Else
                                        consumeCh
                                    End If

                                Loop
                                
                            Else
                                MsgBox "Missing } for subscript"
                                End
                            End If
                        Else
                            .Characters(curCh + 1).Font.Subscript = True
                            consumeCh
                            MathFormat curCh, curCh

                        End If
                    End If
                Else
                    consumeCh
                End If
                If curCh > endPos Then Exit Do
                
            Loop
            
        End If
    End With
    
End Sub

Public Sub Formating()
    Dim nextCh As String
    Dim startPos As Long, endPos As Long
    init
    'DumpSelect
    Do While hasNextCh
        nextCh = getNextCh
        Select Case nextCh
        Case "\"
            consumeCh
            If hasNextCh Then
                nextCh = getNextCh
                If nextCh = "$" Then
                    AddDeleteCh curCh
                    consumeCh
                End If
            End If
        Case "$"
            consumeCh
            AddDeleteCh curCh
            startPos = curCh
            endPos = startPos
            Do While hasNextCh
                nextCh = getNextCh
                If nextCh = "\" Then
                    consumeCh
                    AddDeleteCh curCh
                    If hasNextCh Then
                        If getNextCh() = "$" Then
                            consumeCh
                        End If
                    End If
                    
                ElseIf nextCh = "$" Then
                    consumeCh
                    AddDeleteCh curCh
                    endPos = curCh - 1
                    curCh = startPos
                    MathFormat startPos, endPos
                    curCh = endPos + 1
                    Exit Do
                Else
                    consumeCh
                End If
            Loop
        Case Else
            consumeCh
        End Select
        
    Loop
    DeleteCh
    
End Sub

Private Sub DumpSelect()
    Dim i As Integer
    With ActiveWindow.Selection
        For i = 1 To .Characters.Count
            Debug.Print i; ": |" + .Characters(i).Text + "|"
        Next i
    End With
    
End Sub
